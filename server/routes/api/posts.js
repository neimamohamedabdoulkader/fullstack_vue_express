const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
let postsCollection;

// Connect to the database
(async () => {
  const client = await mongodb.MongoClient.connect('mongodb+srv://neima:1234@nodeexpressprojects.4lcxdow.mongodb.net/vue_express', {
    useNewUrlParser: true,
  });
  const db = client.db('vue_express');
  postsCollection = db.collection('posts');
})();

// GET 
router.get('/', async (req, res) => {
  const posts = await loadPostsCollection();
  res.send(await posts.find({}).toArray());
});

// ADD post
router.post('/', async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.insertOne({
    text: req.body.text,
    createdAt: new Date(),
  });
  res.status(201).send();
});

// DELETE
router.delete('/:id', async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.deleteOne({ _id: new mongodb.ObjectId(req.params.id) });
  res.status(200).send();
});

async function loadPostsCollection() {
  return postsCollection;
}

module.exports = router;
